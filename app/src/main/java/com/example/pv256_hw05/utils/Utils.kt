package com.example.pv256_hw05.utils

import com.example.pv256_hw05.database.models.User

object Generator {
    private val names = listOf(
        "John", "Emma", "Michael", "Sophia", "William",
        "Olivia", "James", "Ava", "Alexander", "Isabella",
        "Ethan", "Mia", "Daniel", "Emily", "Benjamin",
        "Charlotte", "Logan", "Amelia", "Matthew", "Harper"
    )

    fun generateName() = names.random()

    private fun generateAge() = (18..60).random()

    fun generateUser() = User(nickName = generateName(), age = generateAge())
}