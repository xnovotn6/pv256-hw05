package com.example.pv256_hw05.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    val userId: Int = 0,
    val nickName: String,
    val age: Int
)