package com.example.pv256_hw05.ui

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pv256_hw05.database.AppDatabase
import com.example.pv256_hw05.database.models.User
import com.example.pv256_hw05.repository.UserRepository

// TODO #1: Create a ViewModel for the User entity.
class UserViewModel(repository: UserRepository) : ViewModel() {
    // TODO #2: Create a MutableStateFlow of UsersUiState and its unmodifiable version
    //
    // TODO #3: Create a function "insert" which inserts a user into the database (using repository).
    //  User is passed as a parameter.

    // TODO #4: Create a function "fetchUsers" which fetches all users from the database (using repository).
    //  TIP: It should update the _uiState with the fetched users.

    // TODO #5: Create a function "deleteAllUsers" which deletes all users from the database (using repository).
}

sealed class UsersUiState {
    data object Loading : UsersUiState()
    data object Empty : UsersUiState()
    data class Success(val users: List<User>) : UsersUiState()
    data class Error(val message: String) : UsersUiState()
}

class UserViewModelFactory(private val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            val repository = UserRepository(AppDatabase.getInstance(context).userDao())
            @Suppress("UNCHECKED_CAST") return UserViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
