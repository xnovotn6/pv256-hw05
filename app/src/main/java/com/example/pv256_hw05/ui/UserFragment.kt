package com.example.pv256_hw05.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.pv256_hw05.databinding.FragmentUserBinding

class UserFragment : Fragment() {

    private var _binding: FragmentUserBinding? = null
    private val binding get() = _binding!!

    // TODO #6: Initialize the viewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentUserBinding.inflate(inflater, container, false)
        bindViews()
        return binding.root
    }

    // TODO #7: Implement function that will bind the TextView and the buttons
    //  Careful when binding the TextView - you could leak data if you don't handle the lifecycle properly
    //  In textView, show text according to the state of the UI
    private fun bindViews() {}

    // TODO #8 (Don't push to repo, just answer) Unbind the "getAllUsers" button "UserViewModel",
    //  return to the "UserViewModel.kt" and call the "fetchUsers" function in the "init" block.
    //  Why does it work? Do you need "getAllUsers" button in the App?
}

