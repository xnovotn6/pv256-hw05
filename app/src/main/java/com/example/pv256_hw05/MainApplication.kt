package com.example.pv256_hw05

import android.app.Application
import com.example.pv256_hw05.database.AppDatabase

class MainApplication : Application() {
    init {
        AppDatabase.getInstance(this)
    }
}