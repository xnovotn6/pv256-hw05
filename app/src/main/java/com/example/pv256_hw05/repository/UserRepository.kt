package com.example.pv256_hw05.repository

import com.example.pv256_hw05.database.dao.UserDao
import com.example.pv256_hw05.database.models.User
import kotlinx.coroutines.flow.Flow


class UserRepository(private val userDao: UserDao) {

    suspend fun insert(user: User) = userDao.insert(user)

    fun getAllUsers(): Flow<List<User>> = userDao.getAllUsers()

    suspend fun deleteAllUsers() = userDao.deleteAll()
}